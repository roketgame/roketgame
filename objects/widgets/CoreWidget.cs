﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RoketGame
{

    /* widget sahnede bir SceneObject olarak davranir. Bu haliyle bir SceneObjecttir ama icindeki canvas ve elementleri ui elementi gibi davranir */
    public class CoreWidget : CoreSceneObject
    {
        private Canvas canvas;

        public override void Init()
        {
            base.Init();

            canvas = GetComponentInChildren<Canvas>();
        }

        public override void OnTouch(RoketGame.Touch _info)
        {
            base.OnTouch(_info);
        }
    }
}