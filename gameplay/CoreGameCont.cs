﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace RoketGame
{
    [System.Serializable]
    public class Config
    {
        public static Config Instance { get { return (CoreGameCont.Instance).ConfigCore; } }
        public int DebugMode = 0;
        public int DebugLogLevel = 0;
        /* CoreSceneCont.SpawnItem() 'in pooling ozelligini kullanip kullanmayacagi  */
        public bool UsingPoolSpawning = true;
    }


    [RequireComponent(typeof(CoreSceneCont))]
    public class CoreGameCont : CoreCont<CoreGameCont>
    {

        [HideInInspector] public bool IsStartedGame;
        public string Version;
        public Config ConfigCore ;
  
     
        [HideInInspector] public CoreSceneCont sceneCont;
        [HideInInspector] public CoreUiCont uiCont;
        private CoreInputCont inputCont;
        [HideInInspector] public CorePlayerCont playerCont;
  
       



        void Awake()
        {
            Init();
        }


        public override void Init()
        {
            base.Init();


            inputCont = GetComponent<CoreInputCont>();
            if (inputCont == null) inputCont = gameObject.AddComponent<CoreInputCont>();
            if (GetComponent<DebugScreen>() == null) gameObject.AddComponent<DebugScreen>();
            uiCont = FindObjectOfType<CoreUiCont>();
            playerCont = FindObjectOfType<CorePlayerCont>();
            sceneCont = FindObjectOfType<CoreSceneCont>();



            if (!uiCont) Debug.LogWarning("UiCont not found!");
            if (!playerCont) Debug.LogError("PlayerCont not found!");
            if (!sceneCont) Debug.LogError("SceneCont not found!");
            if (!sceneCont) Debug.LogError("InputCont not found!");

            inputCont.Init();



            uiCont.EventGameStatus.AddListener(uiContOnHandler);
            uiCont.Init();

            sceneCont.Init();

            playerCont.Init();

            uiCont.Load(); //trigger!

            //if (Application.platform = RuntimePlatform.Android || Application.platform == RuntimePlatform.IPhonePlayer)
            if (RoketGame.Config.Instance.DebugMode < 2)
                FindObjectOfType<DebugScreen>().enabled = false;
        }


        private void uiContOnHandler(GameStatus _status)
        {
            if (_status == GameStatus.LOADED)
                StartGame();
        }

        public override void StartGame()
        {


            IsStartedGame = true;
            base.StartGame();
            sceneCont.StartGame();
            uiCont.StartGame();
            playerCont.StartGame();
            foreach (CoreObject obj in FindObjectsOfType<CoreObject>() as CoreObject[])
            {
                if (obj.GameStatus != GameStatus.STARTGAME)
                {
                    obj.StartGame();
                }

            }

        }

        public void RestartGame()
        {
            if (ConfigCore.DebugLogLevel > 0) Debug.Log(name + "-> RestartGame()");
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
            //int sceneIndex = SceneManager.GetActiveScene().buildIndex;
            //StartCoroutine(LoadYourAsyncScene(sceneIndex));
        }

        private IEnumerator LoadYourAsyncScene(int index)
        {
            AsyncOperation asyncLoad = SceneManager.LoadSceneAsync(index);
            while (!asyncLoad.isDone)
            {
                yield return null;
            }

            //Init();
        }



        public virtual int GetGameResult()
        {
            return -1;
        }

        public virtual int GameOver()
        {
            int result = GetGameResult();
            if (result > -1)
            {
                if (IsStartedGame)
                {
                    IsStartedGame = false;
                    if (RoketGame.Config.Instance.DebugLogLevel > 0) Debug.Log(name + "-> GameOver(" + result + ")");
                }



            }
            return result;
        }
    }
}