﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;


namespace RoketGame
{
    public class CoreInputCont : CoreCont<CoreInputCont>
    {

        private Touch touchStart;
        private float touchStartTime;
        /* EventTap : Ekranin herhangi bir yerine bir kez dokunulmasi */
        public EventTouch EventTouch =  new RoketGame.EventTouch();
        /* EventSwipe : Ekranin herhangi bir yerinde bir swipe yapıldiginda */
        public EventSwipe EventSwipe = new RoketGame.EventSwipe();
        private Plane virtualPlane;
        public override void Init()
        {
            virtualPlane = new Plane(Vector3.up, Vector3.zero);
            base.Init();
        }


        public override void Update()
        {
            if (Application.platform == RuntimePlatform.WindowsEditor || Application.platform == RuntimePlatform.OSXEditor)
            {
                if (UnityEngine.Input.GetMouseButtonDown(0))
                {
                    startTouch(UnityEngine.Input.mousePosition);
                    //endTouch(Input.mousePosition);
                }
                if (UnityEngine.Input.GetMouseButtonUp(0))
                {
                    //startTouch(Input.mousePosition);
                    endTouch(UnityEngine.Input.mousePosition);
                }
                if (UnityEngine.Input.GetMouseButton(0))
                {
                    //startTouch(Input.mousePosition);
                    updateTouch(UnityEngine.Input.mousePosition);
                }
            }
            else
            {
                if (UnityEngine.Input.touchCount > 0)
                {
                    UnityEngine.Touch touch = UnityEngine.Input.GetTouch(0);

                    if (touch.phase == TouchPhase.Began)
                    {
                        Vector2 pos = touch.position;
                        startTouch(pos);

                    }
                    if (touch.phase == TouchPhase.Ended)
                    {
                        Vector2 pos = touch.position;
                        endTouch(pos);

                    }

                    if (touch.phase == TouchPhase.Moved)
                    {
                        Vector2 pos = touch.position;
                        updateTouch(pos);

                    }

                }
            }
        }



        protected RoketGame.Touch startTouch(Vector2 _screenPos)
        {
            touchStart = new RoketGame.Touch();
            touchStart.Reset();
            touchStart.Points[0] = Camera.main.ScreenToViewportPoint(_screenPos);
            touchStart.ScenePoints[0] = getOnPlaneHit(_screenPos);
            touchStart.Phase = TouchPhase.Began;
            EventTouch.Invoke(touchStart);
            updateUiAndScene(touchStart);
            touchStartTime = Time.time;
            return touchStart;

        }

        protected RoketGame.Touch updateTouch(Vector2 _screenPos)
        {
            touchStart.Points[1] = Camera.main.ScreenToViewportPoint(_screenPos);
            touchStart.ScenePoints[1] = getOnPlaneHit(_screenPos);
            touchStart.DeltaTime = (Time.time - touchStartTime);
            touchStart.Phase = TouchPhase.Moved;
            EventTouch.Invoke(touchStart);
            //Debug.Log("touchStartTime DeltaTime  " + touchEnd.DeltaTime);
            updateUiAndScene(touchStart);
            return touchStart;
        }

        protected RoketGame.Touch endTouch(Vector2 _screenPos)
        {
            //Input.Touch touchEnd = new Input.Touch();
            //touchEnd.Reset();
            //touchEnd.Points[0] = touchStart;
            touchStart.Points[1] = Camera.main.ScreenToViewportPoint(_screenPos);
            //touchEnd.ScenePoints[0] = getOnPlaneHit(touchStart);
            touchStart.ScenePoints[1] = getOnPlaneHit(_screenPos);
            touchStart.DeltaTime = (Time.time - touchStartTime);
            touchStart.Phase = TouchPhase.Ended;
            EventTouch.Invoke(touchStart);
            EventSwipe.Invoke(touchStart);
            //Debug.Log("touchStartTime DeltaTime  " + touchEnd.DeltaTime);
            updateUiAndScene(touchStart);
            return touchStart;
        }

        private void updateUiAndScene(RoketGame.Touch _tap)
        {
            bool callbackFromUI = false;//ui'de bir touch olmus mu?
            if (CoreUiCont.Instance)
            {
                callbackFromUI = CoreUiCont.Instance.UpdateTouch(_tap);
            }

            if (!callbackFromUI)
            {
                CoreSceneCont.Instance.UpdateTouch(_tap);
            }
        }



        private Vector3 getOnPlaneHit(Vector2 _screenPos)
        {
            Vector3 r = Vector3.zero;
            Ray ray = Camera.main.ScreenPointToRay(new Vector3(_screenPos.x, _screenPos.y, 0));
            float enter = 0.0f;
            if (virtualPlane.Raycast(ray, out enter))
            {
                Vector3 hitPoint = ray.GetPoint(enter);
                r = hitPoint;

            }
            return r;
        }


    }
}