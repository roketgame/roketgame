﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RoketGame
{
    public class CoreCont<T> : CoreObject where T : CoreObject
    {
        private static T _instance;

        public static T Instance
        {
            get
            {

                if (_instance == null)
                    _instance = (T)FindObjectOfType(typeof(T));

                if (_instance == null)
                    Debug.LogWarning("[Singleton] Instance not found!");

                return _instance;

            }
        }


   

    }
}
 